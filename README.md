# MongoDB-Fatec

Série de slides e exemplos usados no minicurso de MongoDB ministrado na Fatec Presidente Prudente em 2019

Disponibilizo por este meio as palestras (com seus acertos e seus erros) tal como foram ministradas durante o miniscurso realizado entre os dias 21 e 24 de outubro de 2019.

Destina-se a pessoal de T.I. e afins que buscam uma síntese de que é MongoDB e sua aplicabilidade.

Possui um resumo histórico sobre a evolução dos bancos de dados assim como uma descrição comparativa dos bancos de dados NoSQL (NoRel).

Contêm exemplos práticos usando o software Robo 3T (Anteriormente chamado de RoboMongo) que pode ser baixado livremente em https://robomongo.org/download

Finaliza com uma sequência de slides de desenvolvimento usando PHP e uma pasta contendo esses exemplos.

Há uma máquina virtual disponível com tudo o que é necessário para rodar MongoDB e PHP. As instruções de como criar essa máquina estão em documento adjunto.

Com o sabor da tarefa cumplida e desejando que seja de ajuda, receba um grande abraço

**Esteban D.Dortta**
Esteban@InovacaoSistemas.com.br |
www.InovacaoSistemas.com.br |
www.YeAPF.com |